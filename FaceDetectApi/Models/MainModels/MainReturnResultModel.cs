﻿using FaceDetectApi.Models.SubModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FaceDetectApi.Models.MainModels
{
    public class MainReturnResultModel
    {
        public ReturnResultModel ReturnResult { get; set; }
    }
}
