﻿using FaceDetectApi.Models.SubModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FaceDetectApi.Models.MainModels
{
    public class FaceDetectResponseModel
    {
       public CustomerInfo CustomerInfo { get; set; }
       public ReturnResultModel ReturnResult { get; set; }

    }

    public class CustomerInfo
    {
        public string CustomerNo { get; set; }
        public string CustomerName { get; set; }
        public List<CustomerFlag> CustomerFlag { get; set; }
    }

    public class CustomerFlag
    {
        public string FlagCode { get; set; }
        public string FlagName { get; set; }
        public string FlagType { get; set; }
        public string FlagDesc { get; set; }

    }
}
