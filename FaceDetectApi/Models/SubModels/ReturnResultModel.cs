﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FaceDetectApi.Models.SubModels
{
    public class ReturnResultModel
    {
        public string Result { get; set; }
        public string ResultCode { get; set; }
        public string ResultDescription { get; set; }
        public DateTime ResultTimestamp { get; set; }
    }
}
