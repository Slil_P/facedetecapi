﻿using FaceDetectApi.Models.MainModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FaceDetectApi.Models.Parameter
{

    public class FaceDetectPara
    {
        public byte[] PictureByte { get; set; }
        public string ChannelSource { get; set; }
        public int Count { get; set; }
    }

    public class VideoWalltPara
    {
        public AnalystInfo AnalystInfo { get; set; }
        public string ChannelSource { get; set; }
        public DateTime Timestamp { get; set; }
        
    }

    public class AnalystInfo
    {
        public int CountMale { get; set; }
        public int CountFemale { get; set; }
        public int Count { get; set; }
        public double SumAge { get; set; }
        public int Num { get; set; }
    }

    public class PostResultPara
    {
        public string CitizenId { get; set; }
        public string CustomerName { get; set; }
        public string CustomerBirthday { get; set; }
        public int CameraID { get; set; }
        public int CameraAreaID { get; set; }
    }

    public class CommandFaceDetectPara
    {
        public string UserKeyAction { get; set; }
        public string ChannelSource { get; set; }
        public DateTime Timestamp { get; set; }

    }
}
