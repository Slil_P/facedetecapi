﻿using FaceDetectApi.Models.MainModels;
using FaceDetectApi.Models.Parameter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace FaceDetectApi.BSL.Interface
{
    public interface  IFaceDetectService
    {

        FaceDetectResponseModel SendFaceDetectData(byte[] image, string channel , int i);

        MainReturnResultModel ReturnFaceDetectData(PostResultPara returnResult );

        MainReturnResultModel CommandFaceDetectByClick(CommandFaceDetectPara command);

        MainReturnResultModel VideoWallFaceDetect(VideoWalltPara postVideo);

        //MainReturnResultModel OpenCloseFaceDetect(CommandFaceDetectPara command);

    }
}
