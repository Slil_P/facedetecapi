﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using FaceDetectApi.BSL.Interface;
using FaceDetectApi.Models.MainModels;
using FaceDetectApi.Models.Parameter;
using Microsoft.AspNetCore.Mvc;

namespace FaceDetectApi.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class FaceDetectController : ControllerBase
    {

        private  IFaceDetectService faceDetectService;

        public FaceDetectController (IFaceDetectService faceDetectService)
        {
            this.faceDetectService = faceDetectService;
        }

        [HttpPost]
        [Route("SendFaceDetectData")]
        [Produces("application/json")]
        public async Task<ActionResult<FaceDetectResponseModel>> SendFaceDetectDataData([FromBody] FaceDetectPara para)
        {
            return faceDetectService.SendFaceDetectData(para.PictureByte , para.ChannelSource , para.Count);

        }

        //[HttpPost]
        //[Route("ReturnFaceDetectData")]
        //[Produces("application/json")]
        //public async Task<ActionResult<MainReturnResultModel>> ReturnFaceDetectData([FromBody] PostResultPara para)
        //{
        //    return faceDetectService.ReturnFaceDetectData(para);
        //}

        [HttpPost]
        [Route("CommandFaceDetectByClick")]
        [Produces("application/json")]
        public async Task<ActionResult<MainReturnResultModel>> CommandFaceDetectByClick([FromBody] CommandFaceDetectPara para)
        {
            return faceDetectService.CommandFaceDetectByClick(para);
        }

        [HttpPost]
        [Route("VideoWallFaceDetect")]
        [Produces("application/json")]
        public async Task<ActionResult<MainReturnResultModel>> VideoWallFaceDetect([FromBody]VideoWalltPara postVideo)
        {
            return faceDetectService.VideoWallFaceDetect(postVideo);
        }

        //[HttpPost]
        //[Route("OpenCloseFaceDetect")]
        //[Produces("application/json")]
        //public async Task<ActionResult<MainReturnResultModel>> OpenCloseFaceDetect([FromBody]CommandFaceDetectPara command)
        //{
        //    return faceDetectService.OpenCloseFaceDetect(command);
        //}


    }
}